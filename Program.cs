using System;

public class Program
{
    public static void Main(string[] args)
    {
        int x = 5;
        int y = 0;
        
        try
        {
            int result = x / y;
            Console.WriteLine("Result: " + result);
        }
        catch (DivideByZeroException e)
        {
            Console.WriteLine("Error: Division by zero!");
        }
        
        string str = null;
        Console.WriteLine("Length of the string: " + str.Length);
    }
}
